###THESE SKETCHES ARE ABSOLUTELY IDENTICAL

These sketches reads map from the text file and generates shortest (doesn't mean optimal) way from the start to the finish.
Way format: 
`List<character, integer>` = `List<direction, length>` 
`direction: {'N', 'S', 'E', 'W'}` 
`0 < length < 200`

|Name|Language|
|:---- | :-------- |
|Solution.cpp	|	C++11|
|Solution.java	|	Java8|
|Solution.py	|	Python2.7|
